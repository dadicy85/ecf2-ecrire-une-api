<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pays;

class PaysController extends Controller
{
    public function index()
    {
        return Pays::all();
    }

    public function show($id)
    {
        return Pays::find($id);
    }

}

